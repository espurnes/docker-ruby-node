# Ruby + Node Docker Image #
This repository contains Docker images based on [starefossen/ruby-node:alpine](https://github.com/Starefossen/docker-ruby-node).

## Why I use this image?
I'm using this image to develop jekyll sites. I'm using nodejs to build, minify css, js svg and serve jekyll, so it's useful to have this image and use it in any project and machine I want. No need to install nodejs and update ruby machine version.


## How to use this image
From your app directory execute the following commads:

### If you want to access to the Alpine terminal
```
$ docker run -it --rm -v "$PWD":/usr/src/app -p "3000:3000" -w="/usr/src/app" espurnes/ruby-node:alpine-1.1 /bin/ash
```

First time I use this command and inside the container I run
```
bundle install
```
and
```
npm install
```
to download all the dependencies.

And then to build and serve the jekyll site I use
```
npm run gulp
```

### If you want to execute npm run gulp
Once I have all the dependencies I don't need to access to the container terminal. I can execute npm run gulp from the local shell like so:

```
$ docker run -it --rm -v "$PWD":/usr/src/app -p "3000:3000" -w="/usr/src/app" espurnes/ruby-node:alpine-1.1 npm run gulp
```

### Documentation

* [Docker](http://docs.docker.com)
* [Ruby](https://www.ruby-lang.org/en/)
* [Node.js](https://nodejs.org/en/)
