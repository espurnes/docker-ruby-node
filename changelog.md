# Version 1.1
**Changes since v1.0**

- Added imagemagick imagemagick-dev. They are needed by jekyll-minimagick. I'm using [jekyll-minimagick](https://github.com/zroger/jekyll-minimagick) in some projects to create image pressets.

***jekyll-minimagick** gem allows you to easily use MiniMagick to crop and resize images in your Jekyll project, according to a preset defined in your config file.*
