# espurnes/ruby-node:alpine-1.1

FROM starefossen/ruby-node:alpine
RUN apk update && apk add ruby-dev alpine-sdk && apk add imagemagick imagemagick-dev
ENV BUNDLE_PATH=/usr/src/app/.bundle/bundle
